var express = require('express');
var router = express.Router();
var time = require('../controllers/time_controller');

router.get('/', time.index);
router.get('/:time', time.parse);

module.exports = router;