const MONTHS = ['January','February','March','April','May','June','July','August','September','October','November','December'];
exports.index = function(req, res) {
    res.render('index');
};
exports.parse = function(req, res) {
    var time = req.params.time;
    if (!time) {
        return res.send('not given time string');
    }
    if (/^\d+$/.test(time)) {
        time = parseInt(time,10) * 1000;
    }
    time = new Date(time);
    var unix = time.getTime();
    if (isNaN(unix)) {
        return res.send('invalid time');
    }
    res.send({
        unix : Math.floor(unix / 1000),
        natural : MONTHS[time.getMonth()] + ' ' + time.getDate() + ', ' + time.getFullYear()
    });

    
};