var log4js = require('log4js');
var slogger = require('node-slogger');

var traceLogger;

log4js.configure({
    appenders: [
        {type: 'console',category:'console'}
    ],
    replaceConsole: true
});
traceLogger = exports.tracelogger = log4js.getLogger('console');

slogger.init({
    disableCustomConsole :true
});



